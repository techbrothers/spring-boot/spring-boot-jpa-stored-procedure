/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.techbrothers.techsolutions.springbootjpastoredprocedure.domain;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.Table;

/**
 *
 * @author chiru
 */
@Entity
@Table(name = "table1")
@NamedStoredProcedureQueries({
        @NamedStoredProcedureQuery(name = "procedure1", procedureName = "new_procedure", resultClasses = User.class)
})
public class User implements Serializable{

    @Override
    public String toString() {
        return "User{" + "name=" + name + ", age=" + age + ", gender=" + gender + '}';
    }
    
    @Id
    private String name;
    
    private String age;
    
    private String gender;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
    
}
