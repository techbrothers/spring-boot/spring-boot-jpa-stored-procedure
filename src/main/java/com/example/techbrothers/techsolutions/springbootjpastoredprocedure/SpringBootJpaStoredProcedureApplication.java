package com.example.techbrothers.techsolutions.springbootjpastoredprocedure;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootJpaStoredProcedureApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootJpaStoredProcedureApplication.class, args);
	}

}
