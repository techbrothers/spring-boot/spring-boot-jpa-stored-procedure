/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.techbrothers.techsolutions.springbootjpastoredprocedure.controller;

import com.example.techbrothers.techsolutions.springbootjpastoredprocedure.dao.StoredProcedureDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author chiru
 */
@RestController
public class StoredProcedureController {
    
    @Autowired
    StoredProcedureDao storedProcedureDao;
    
    @GetMapping("/data")
    public void getData(){
        storedProcedureDao.getData();
    }
    
}
