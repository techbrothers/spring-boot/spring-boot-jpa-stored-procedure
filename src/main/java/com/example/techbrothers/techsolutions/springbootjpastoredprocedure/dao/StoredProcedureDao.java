/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.techbrothers.techsolutions.springbootjpastoredprocedure.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import org.springframework.stereotype.Service;

/**
 *
 * @author chiru
 */
@Service
public class StoredProcedureDao {
    
    @PersistenceContext
    EntityManager entityManager;
    
    public void getData(){
        StoredProcedureQuery storedProcedureQuery = entityManager.createNamedStoredProcedureQuery("procedure1");
        storedProcedureQuery.getResultList().stream().forEach( user -> System.out.println(user));
    }
    
}
